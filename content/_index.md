---
title: "Projeto"
summary: "Este site é um projeto para a disciplina PMR3304. O enunciado do projeto é a criação de um site estático utilizando a ferramenta Hugo."
date: 2022-09-21T12:46:25-03:00
draft: false
---

Neste site, foi criado um blog sobre o meu intercâmbio, com algumas informações sobre a faculdade em que vou estudar: Technische Universität München (TUM). Aqui, você encontrará:

- Minha biografia
- Informações sobre a TUM
- Informações sobre o duplo diploma