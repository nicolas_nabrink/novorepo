---
title: "Sobre"
summary: "Esta página contém informações sobre minha humilde pessoa."
date: 2022-09-21T12:46:25-03:00
draft: false
---
Meu nome é Nicolas, sou estudante do sexto periodo de Engenharia Mecatrônica da Escola Politécnica de Engenharia da USP. No ano de 2023, pretendo fazer o intercâmbio de duplo diploma oferecido pela Escola Politécnica para a TUM. Durante o intercâmbio, estudarei no programa de "Master in Science Mechatronik", no campus de Garching.

